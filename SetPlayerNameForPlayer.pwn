SetPlayerNameForPlayer(playerid, toplayerid, name[])
{
    new BitStream:bs = BS_New(), size = strlen(name);

    BS_WriteValue(
        bs,
        PR_UINT16, playerid,
        PR_UINT8, size,
        PR_STRING, name
    );

    BS_RPC(bs, toplayerid, 11);
    BS_Delete(bs);
    return 1;
} 