SetPlayerSkinForPlayer(playerid, forplayerid, skin)
{
    new BitStream:bs = BS_New();

    BS_WriteValue(
        bs,
        PR_UINT16, playerid,
        PR_UINT32, skin
    );

    BS_RPC(bs, forplayerid, 153, PR_LOW_PRIORITY, PR_RELIABLE_ORDERED);
    BS_Delete(bs);
    return 1;
} 