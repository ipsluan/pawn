RemovePlayerAttObjForPlayer(playerid, toplayerid, index)
{
    new BitStream:bs = BS_New();

    BS_WriteValue(
        bs,
        PR_UINT16, playerid,
        PR_UINT32, index,
        PR_BOOL, 0
    );

    BS_RPC(bs, toplayerid, 113, PR_LOW_PRIORITY, PR_RELIABLE_ORDERED);
    BS_Delete(bs);
    return 1;
} 