RemovePlayerForPlayer(playerid, toplayerid) 
{ 
    new BitStream:bs = BS_New(); 

    BS_WriteValue( 
        bs, 
        PR_UINT16, playerid
    ); 

    BS_RPC(bs, toplayerid, 163, PR_LOW_PRIORITY, PR_RELIABLE_ORDERED); 
    BS_Delete(bs); 
    return 1; 
} 