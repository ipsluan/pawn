SetPlayerAttObjForPlayer(playerid, toplayerid, index, modelid, bone, Float:fOffsetX = 0.0, Float:fOffsetY = 0.0, Float:fOffsetZ = 0.0, Float:fRotX = 0.0, Float:fRotY = 0.0, Float:fRotZ = 0.0, Float:fScaleX = 1.0, Float:fScaleY = 1.0, Float:fScaleZ = 1.0, materialcolor1 = 0, materialcolor2 = 0)
{
    new BitStream:bs = BS_New();

    BS_WriteValue(
        bs,
        PR_UINT16, playerid,
        PR_UINT32, index,
        PR_BOOL, 1,
        PR_UINT32, modelid,
        PR_UINT32, bone,
        PR_FLOAT, fOffsetX,
        PR_FLOAT, fOffsetY,
        PR_FLOAT, fOffsetZ,
        PR_FLOAT, fRotX,
        PR_FLOAT, fRotY,
        PR_FLOAT, fRotZ,
        PR_FLOAT, fScaleX,
        PR_FLOAT, fScaleY,
        PR_FLOAT, fScaleZ,
        PR_UINT32, materialcolor1,
        PR_UINT32, materialcolor2
    );

    BS_RPC(bs, toplayerid, 113, PR_LOW_PRIORITY, PR_RELIABLE_ORDERED);
    BS_Delete(bs);
    return 1;
} 