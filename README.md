# Explicação dos códigos

SetPlayerAttObjForPlayer- Anexa um objeto em um jogador apenas para outro jogador.<br/>
RemovePlayerAttObjForPlayer- Remove um objeto anexado em um jogador apenas para outro jogador.<br/>
SetPlayerSkinForPlayer - Muda o skin de um jogador para outro jogador.

# Detalhes importantes

Ex: SetPlayerSkinForPlayer - Quando o jogador entrar na zona de streaming de outro jogador, o servidor manda o RPC WorldPlayerAdd com dados como estílo de luta, skin, cor, etc. <br/>
Você vai ter que mudar esses dados com os que você vai utilizar nas funções por-jogador, ou você verá o jogador com sua informação original (skin, cor, etc).<br/>
Vamos dizer que você tem o skin por-jogador salvo em uma matriz: new SkinPorJogador[MAX_PLAYERS][MAX_PLAYERS];<br/>
Você vai ter que tomar conta de casos em que o servidor pode mudar o skin para todos (ex: SetPlayerSkin). <br/>
Para isso, você vai utilizar a callback OnOutcomingRPC e mudar o valor da matriz com o novo ID de skin dado pelo servidor no BitStream (RPC 153), assim, a matriz SkinPorJogador terá o valor dado pelo servidor quando mudado.